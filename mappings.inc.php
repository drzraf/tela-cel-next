<?php

// RechercheObservation.php

function _p() {
	return Cel::db()->proteger(func_get_args());
}

[
	// bigint(20)
	'id_observation' => [ 'outalias' => 'oid', 'inalias' => ['id','idobs'], 'p' => function($i) { return intval($i); } ],
	'ordre' => [],

	'ce_utilisateur' => [ 'outalias' => 'uid', 'inalias' => [ 'uid' ], 'p' => function($i) { return _p($i); } ],
];

/*
  ce_utilisateur varchar(32)
	prenom_utilisateur varchar(255)
	nom_utilisateur varchar(255)
	courriel_utilisateur varchar(255)
	nom_sel varchar(601)
	nom_sel_nn decimal(9,0)
	nom_ret varchar(601)
	nom_ret_nn decimal(9,0)
	nt decimal(9,0)
	famille varchar(255)
	nom_referentiel varchar(255)
	ce_zone_geo varchar(50)
	zone_geo varchar(255)
	lieudit varchar(255)
	station varchar(255)
	milieu varchar(255)
	latitude decimal(8,5)
	longitude decimal(8,5)
	altitude int(5)
	geodatum varchar(25)
date_observation datetime


// longtext
// mots_cles_texte
commentaire text
	transmission tinyint(1)
date_creation datetime
date_modification datetime
date_transmission datetime
abondance varchar(50)
certitude varchar(255)
	phenologie varchar(255)
code_insee_calcule varchar(5)

*/


[
	'annee' => [ 'outalias' => 'annee', 'inalias' => ['annee'], 'q' => function($i) {
			return $q ? sprintf('YEAR(date_observation) = %s', _p($i)) :
			"(date_observation IS NULL OR YEAR(date_observation) = 0000)" ;
		} ],

	'mois' => [ 'outalias' => 'mois', 'inalias' => ['mois'], 'q' => function($i) {
			return $q ? sprintf('MONTH(date_observation) = %s', _p($i)) :
			"(date_observation IS NULL OR MONTH(date_observation) = 00)" ;
		} ],


]
