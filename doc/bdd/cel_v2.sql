SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `cel_obs_images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_obs_images` ;

CREATE  TABLE IF NOT EXISTS `cel_obs_images` (
  `id_image` BIGINT NOT NULL ,
  `id_observation` BIGINT NOT NULL ,
  `date_liaison` DATETIME NOT NULL ,
  PRIMARY KEY (`id_image`, `id_observation`) ,
  INDEX `observation` (`id_observation` ASC) ,
  INDEX `image` (`id_image` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `cel_utilisateurs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_utilisateurs` ;

CREATE  TABLE IF NOT EXISTS `cel_utilisateurs` (
  `id_utilisateur` INT NOT NULL ,
  `prenom` VARCHAR(255) NULL DEFAULT NULL ,
  `nom` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `courriel` VARCHAR(255) NOT NULL ,
  `mot_de_passe` VARCHAR(45) NOT NULL ,
  `admin` TINYINT(1) NULL DEFAULT '0' ,
  `licence_acceptee` TINYINT(1) NULL DEFAULT '0' COMMENT 'Acceptation de la licence utilisateur pour le cel\n' ,
  `preferences` LONGTEXT NULL DEFAULT NULL COMMENT 'Préférences utilisateur sérialisées sous une forme à définir\n' ,
  `date__premiere_utilisation` DATETIME NOT NULL ,
  PRIMARY KEY (`id_utilisateur`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `cel_mots_cles_images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_mots_cles_images` ;

CREATE  TABLE IF NOT EXISTS `cel_mots_cles_images` (
  `id_mot_cle_image` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'Identifiant du mot-clé pour une position donnée dans l\'arbre d\'utilisateur.\nLe même mot-clé peut être placé à plusieurs endroits de l\'arbre et aura donc deux id distincts.' ,
  `id_utilisateur` VARCHAR(32) NOT NULL ,
  `mot_cle` VARCHAR(50) NOT NULL COMMENT 'Mot clé de l\'utilisateur pour une position dans l\'arbre.' ,
  `md5` VARCHAR(32) NOT NULL COMMENT 'MD5 du mot clé en minuscule.' ,
  `bg` BIGINT NOT NULL COMMENT 'Bordure gauche.' ,
  `bd` BIGINT NOT NULL COMMENT 'bordure droite.' ,
  `niveau` INT NOT NULL COMMENT 'Niveau du mot clé dans l\'arbre.' ,
  `ce_mot_cle_image_parent` VARCHAR(128) NOT NULL ,
  PRIMARY KEY (`id_mot_cle_image`, `id_utilisateur`) ,
  CONSTRAINT `fk_cel_mots_cles_images_cel_utilisateur1`
    FOREIGN KEY (`id_utilisateur` )
    REFERENCES `cel_utilisateurs` (`id_utilisateur` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cel_mots_cles_images_cel_mots_cles_images1`
    FOREIGN KEY (`ce_mot_cle_image_parent` )
    REFERENCES `cel_mots_cles_images` (`id_mot_cle_image` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Table des mots clés, à utiliser avec des transactions !\n'
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `cel_images_mots_cles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_images_mots_cles` ;

CREATE  TABLE IF NOT EXISTS `cel_images_mots_cles` (
  `id_image` BIGINT NOT NULL ,
  `id_mot_cle_image` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `id_utilisateur` VARCHAR(32) NOT NULL ,
  PRIMARY KEY (`id_image`, `id_mot_cle_image`, `id_utilisateur`) ,
  INDEX `image` (`id_image` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `cel_images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_images` ;

CREATE  TABLE IF NOT EXISTS `cel_images` (
  `id_image` BIGINT NOT NULL AUTO_INCREMENT ,
  `ordre` BIGINT NOT NULL ,
  `ce_utilisateur` VARCHAR(32) NOT NULL COMMENT 'L\'id utilisateur est un int mais on utilise un varchar pour stocker des observations avec des identifiants temporaires\n' ,
  `prenom_utilisateur` VARCHAR(255) NULL DEFAULT NULL ,
  `nom_utilisateur` VARCHAR(255) NULL DEFAULT NULL ,
  `courriel_utilisateur` VARCHAR(255) NULL DEFAULT NULL ,
  `hauteur` INT NOT NULL ,
  `largeur` INT NOT NULL ,
  `appareil_fabriquant` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `appareil_modele` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `date_prise_de_vue` DATETIME NULL DEFAULT NULL ,
  `note_qualite` DECIMAL(1,0) NULL DEFAULT NULL ,
  `mots_cles_texte` LONGTEXT NULL DEFAULT NULL COMMENT 'Champ calculé contenant la liste des mots clés utilisateurs séparé par des virgules.\n' ,
  `commentaire` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `nom_original` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `md5` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `meta_exif` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `meta_iptc` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `meta_xmp` LONGTEXT NULL DEFAULT NULL ,
  `meta_makernote` LONGTEXT NULL DEFAULT NULL ,
  `meta_autres` LONGTEXT NULL DEFAULT NULL ,
  `date_modification` DATETIME NOT NULL ,
  `date_creation` DATETIME NOT NULL COMMENT 'Date d\'ajout de l\'image au CEL.' ,
  `publiable_eflore` TINYINT(1) NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id_image`) ,
  INDEX `id_image` (`id_image` ASC, `ordre` ASC, `ce_utilisateur` ASC) )
ENGINE = MyISAM
AUTO_INCREMENT = 265
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `cel_mots_cles_obs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_mots_cles_obs` ;

CREATE  TABLE IF NOT EXISTS `cel_mots_cles_obs` (
  `id_mot_cle_obs` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `id_utilisateur` VARCHAR(32) NOT NULL ,
  `mot_cle` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `md5` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `bg` BIGINT NOT NULL ,
  `bd` BIGINT NOT NULL ,
  `niveau` INT NOT NULL ,
  `ce_mot_cle_obs_parent` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`id_mot_cle_obs`, `id_utilisateur`) ,
  CONSTRAINT `fk_cel_mots_cles_obs_cel_utilisateur1`
    FOREIGN KEY (`id_utilisateur` )
    REFERENCES `cel_utilisateurs` (`id_utilisateur` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cel_mots_cles_obs_cel_mots_cles_obs1`
    FOREIGN KEY (`ce_mot_cle_obs_parent` )
    REFERENCES `cel_mots_cles_obs` (`id_mot_cle_obs` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Table des mots clés, à utiliser avec des transactions !'
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `cel_obs_mots_cles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_obs_mots_cles` ;

CREATE  TABLE IF NOT EXISTS `cel_obs_mots_cles` (
  `id_observation` BIGINT NOT NULL ,
  `id_mot_cle_obs` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `id_utilisateur` VARCHAR(32) NOT NULL ,
  PRIMARY KEY (`id_observation`, `id_mot_cle_obs`, `id_utilisateur`) ,
  INDEX `observation` (`id_observation` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `cel_zones_geo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_zones_geo` ;

CREATE  TABLE IF NOT EXISTS `cel_zones_geo` (
  `id_zone_geo` VARCHAR(50) NOT NULL ,
  `code` VARCHAR(10) NOT NULL ,
  `nom` VARCHAR(255) NOT NULL ,
  `utm_secteur` CHAR(3) NOT NULL ,
  `utm_x` INT NOT NULL DEFAULT '0' ,
  `utm_y` INT NOT NULL DEFAULT '0' ,
  `wgs84_latitude` FLOAT NOT NULL ,
  `wgs84_longitude` FLOAT NOT NULL ,
  `date_modification` DATETIME NOT NULL ,
  `ce_zone_geo_parente` VARCHAR(50) NULL DEFAULT NULL ,
  `bg` BIGINT NULL ,
  `bd` BIGINT NULL ,
  `niveau` INT NULL ,
  PRIMARY KEY (`id_zone_geo`) ,
  INDEX `nom` (`nom` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `cel_obs_etendues`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_obs_etendues` ;

CREATE  TABLE IF NOT EXISTS `cel_obs_etendues` (
  `id_observation` BIGINT NOT NULL ,
  `cle` VARCHAR(255) NOT NULL COMMENT 'Clé du champ au format chat mot (sans accents).\nEx. : maCle, uneAutreCle' ,
  `label` VARCHAR(255) NOT NULL COMMENT 'Intitulé du champ à afficher dans les formulaires.' ,
  `valeur` TEXT NOT NULL COMMENT 'Valeur du champ.' ,
  PRIMARY KEY (`id_observation`, `cle`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Stockage d\'infos supplémentaires sur une observation';


-- -----------------------------------------------------
-- Table `cel_obs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_obs` ;

CREATE  TABLE IF NOT EXISTS `cel_obs` (
  `id_observation` BIGINT NOT NULL AUTO_INCREMENT ,
  `ordre` BIGINT NOT NULL ,
  `ce_utilisateur` VARCHAR(255) NOT NULL ,
  `prenom_utilisateur` VARCHAR(255) NULL DEFAULT NULL ,
  `nom_utilisateur` VARCHAR(255) NULL DEFAULT NULL ,
  `courriel_utilisateur` VARCHAR(255) NULL DEFAULT NULL ,
  `nom_sel` VARCHAR(601) NULL DEFAULT NULL COMMENT 'doit pouvoir contenir CONCAT(bdtfx.nom_sci, \" \", bdtfx.auteur) soit 601 caractères' ,
  `nom_sel_nn` DECIMAL(9,0) NULL DEFAULT NULL COMMENT 'Numéro du nom sélectionné.' ,
  `nom_ret` VARCHAR(601) NULL DEFAULT NULL COMMENT 'doit pouvoir contenir CONCAT(bdtfx.nom_sci, \" \", bdtfx.auteur) soit 601 caractères' ,
  `nom_ret_nn` DECIMAL(9,0) NULL DEFAULT NULL COMMENT 'Numéro du nom retenu = num_nom_retenu dans bdtfx' ,
  `nt` DECIMAL(9,0) NULL DEFAULT NULL COMMENT 'Numéro taxonomique.' ,
  `famille` VARCHAR(255) NULL DEFAULT NULL ,
  `nom_referentiel` VARCHAR(255) NULL DEFAULT NULL ,
  `ce_zone_geo` VARCHAR(50) NULL DEFAULT NULL ,
  `zone_geo` VARCHAR(255) NULL DEFAULT NULL ,
  `lieudit` VARCHAR(255) NULL DEFAULT NULL ,
  `station` VARCHAR(255) NULL DEFAULT NULL ,
  `milieu` VARCHAR(255) NULL DEFAULT NULL ,
  `latitude` DECIMAL(8,5) NULL DEFAULT NULL ,
  `longitude` DECIMAL(8,5) NULL DEFAULT NULL ,
  `altitude` INT(5) NULL DEFAULT NULL ,
  `geodatum` VARCHAR(25) NULL DEFAULT NULL COMMENT 'Référentiel géographique utilisé.\nPar exmple : WGS84' ,
  `date_observation` DATETIME NULL DEFAULT NULL ,
  `mots_cles_texte` LONGTEXT NULL DEFAULT NULL COMMENT 'Champ calculé contenant la liste des mots clés utilisateurs séparé par des virgules.' ,
  `commentaire` TEXT NULL DEFAULT NULL ,
  `transmission` TINYINT(1) NULL DEFAULT NULL ,
  `date_creation` DATETIME NULL DEFAULT NULL ,
  `date_modification` DATETIME NULL DEFAULT NULL ,
  `date_transmission` DATETIME NULL DEFAULT NULL ,
  `abondance` VARCHAR(50) NULL DEFAULT NULL ,
  `certitude` VARCHAR(255) NULL DEFAULT NULL ,
  `phenologie` VARCHAR(255) NULL DEFAULT NULL ,
  `code_insee_calcule` VARCHAR(5) NULL DEFAULT NULL COMMENT 'Code INSEE calculé par un scrip CRON.' ,
  PRIMARY KEY (`id_observation`) ,
  UNIQUE INDEX `id_obs` (`ce_utilisateur` ASC, `ordre` ASC) ,
  INDEX `date_creation` (`ce_utilisateur`(10) ASC, `date_creation` ASC) ,
  INDEX `coordonnees` (`latitude` ASC, `longitude` ASC) ,
  INDEX `nom_retenu` (`nom_ret` ASC) ,
  INDEX `date_observation` (`date_observation` ASC)
  INDEX `nom_referentiel` (`nom_referentiel`(5) ASC) COMMENT "index sur (bdtfx,bdtfx,isfan)",
  INDEX `date_transmission` (`date_transmission` DESC) COMMENT "date_transmission : nécessaire à l'ORDER BY utilisé dans la liste d'observation de DEL", -- 'relax emacs
  INDEX `transmission` (`transmission` ASC) ) COMMENT "nécessaire à CEL/DEL qui officie avec transmission = 1"
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `cel_utilisateurs_infos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_utilisateurs_infos` ;

CREATE  TABLE IF NOT EXISTS `cel_utilisateurs_infos` (
  `id_utilisateur` INT NOT NULL ,
  `admin` TINYINT(1) NOT NULL DEFAULT 0 ,
  `licence_acceptee` TINYINT(1) NOT NULL DEFAULT 0 ,
  `preferences` LONGTEXT NULL DEFAULT NULL ,
  `date_premiere_utilisation` DATETIME NOT NULL ,
  PRIMARY KEY (`id_utilisateur`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Contient les infos utilisateurs spécifiques au CEL. À utilis /* comment truncated */ /*er avec une vue pour récupérer les infos de la table annuaire_tela.*/';


-- -----------------------------------------------------
-- Table `cel_images_etendues`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cel_images_etendues` ;

CREATE  TABLE IF NOT EXISTS `cel_images_etendues` (
  `id_observation` BIGINT NOT NULL ,
  `cle` VARCHAR(255) NOT NULL COMMENT 'Clé du champ au format chat mot (sans accents).\nEx. : maCle, uneAutreCle' ,
  `label` VARCHAR(45) NOT NULL COMMENT 'Intitulé du champ à afficher dans les formulaires.' ,
  `valeur` TEXT NOT NULL COMMENT 'Valeur du champ.' ,
  PRIMARY KEY (`id_observation`, `cle`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Stockage d\'info supplémentaires sur une image';  -- 'relax emacs


-- -----------------------------------------------------
-- Placeholder table for view `cel_utilisateurs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cel_utilisateurs` (`id_utilisateur` INT, `prenom` INT, `nom` INT, `courriel` INT, `mot_de_passe` INT, `licence_acceptee` INT, `admin` INT, `preferences` INT, `date_premiere_utilisation` INT);

-- -----------------------------------------------------
-- Placeholder table for view `cel_tapir`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cel_tapir` (`guid` INT, `observation_id` INT, `observation_date` INT, `nom_scientifique_complet` INT, `nom_num_nomenclatural` INT, `nom_num_taxonomique` INT, `nom_famille` INT, `lieu_commune_nom_complet` INT, `lieu_commune_nom` INT, `lieu_commune_code_insee` INT, `lieu_commune_source` INT, `lieu_latitude` INT, `lieu_longitude` INT, `lieu_geodatum` INT, `lieu_georeference_source` INT, `lieu_localite` INT, `observateur_prenom` INT, `observateur_nom` INT, `observateur_courriel` INT, `observateur_nom_complet` INT, `observateur_intitule` INT, `observation_commentaire` INT, `observation_information_complement` INT, `saisie_date_modification` INT, `saisie_date_creation` INT);

-- -----------------------------------------------------
-- View `cel_utilisateurs`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `cel_utilisateurs` ;
DROP TABLE IF EXISTS `cel_utilisateurs`;
CREATE  OR REPLACE VIEW `cel_utilisateurs` AS 
	SELECT at.U_ID AS id_utilisateur, at.U_SURNAME AS prenom, at.U_NAME AS nom, at.U_MAIL AS courriel, at.U_PASSWD AS mot_de_passe,  
		ui.licence_acceptee, ui.admin, ui.preferences, ui.date_premiere_utilisation 
	FROM tela_prod_v4.annuaire_tela AS at
		LEFT JOIN cel_utilisateurs_infos AS ui ON (ui.id_utilisateur = at.U_ID);

-- -----------------------------------------------------
-- View `cel_tapir`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `cel_tapir` ;
DROP TABLE IF EXISTS `cel_tapir`;
CREATE  OR REPLACE VIEW `cel_tapir` AS 
	select concat(_utf8'urn:lsid:tela-botanica.org:cel:',`o`.`id_observation`) AS `guid`,
		`o`.`id_observation` AS `observation_id`,
		date_format(`o`.`date_observation`,'%Y-%m-%d') AS `observation_date`,
		`o`.`nom_sel` AS `nom_scientifique_complet`,
		`o`.`nom_sel_nn` AS `nom_num_nomenclatural`,
		`o`.`nt` AS `nom_num_taxonomique`,
		`o`.`famille` AS `nom_famille`,
		concat(_utf8'',`zg`.`nom`,' [INSEE:',`zg`.`code`,']') AS `lieu_commune_nom_complet`,
		`zg`.`nom` AS `lieu_commune_nom`,
		`zg`.`code` AS `lieu_commune_code_insee`,
		if((`zg`.`code` <> ''), 'Lion1906 version 26-05-2008 - http://www.lion1906.com/', NULL) AS `lieu_commune_source`,
		format(if((`o`.`latitude` <> ''), `o`.`latitude`, `zg`.`wgs84_latitude`), 5) AS `lieu_latitude`,
		format(if((`o`.`longitude` <> ''), `o`.`longitude`, `zg`.`wgs84_longitude`), 5) AS `lieu_longitude`,
		`o`.`geodatum` AS `lieu_geodatum`,
		if((`o`.`geodatum` <> ''), 'Coordonnées issues de l''utilisation de Google Map', NULL) AS `lieu_georeference_source`,
		`o`.`lieudit` AS `lieu_localite`,
		`o`.`prenom_utilisateur` AS `observateur_prenom`,
		`o`.`nom_utilisateur` AS `observateur_nom`,
		`o`.`courriel_utilisateur` AS `observateur_courriel`,
		concat(`o`.`prenom_utilisateur`,_utf8' ',`o`.`nom_utilisateur`) AS `observateur_nom_complet`,
		concat_ws(' ',`o`.`prenom_utilisateur`,`o`.`nom_utilisateur`,concat('<',`o`.`courriel_utilisateur`,'>')) AS `observateur_intitule`,
		`o`.`commentaire` AS `observation_commentaire`,
		concat(_utf8'nom_num_nomenclatural=',`o`.`nom_sel_nn`,'; ',
			'nom_ret=',encodeToDcsv(`o`.`nom_ret`),'; ',
			'nom_num_ret=',`o`.`nom_ret_nn`,'; ',
			'nom_num_taxonomique=',`o`.`nt`,'; ',
			'nom_referentiel=',encodeToDcsv(`o`.`nom_referentiel`),'; ',
			'saisie_date_transmission=',`o`.`date_transmission`,'; ',
			'saisie_date_creation=',`o`.`date_creation`,'; ',
			'ordre=',`o`.`ordre`,'; ',
			'station=',encodeToDcsv(`o`.`station`),'; ',
			'milieu=',encodeToDcsv(`o`.`milieu`),'; ',
			'mots_cles=',encodeToDcsv(`o`.`mots_cles_texte`),'; ',
			'zg_utm_secteur=',encodeToDcsv(`zg`.`utm_secteur`),'; ',
			'zg_date_modification=',`zg`.`date_modification`) AS `observation_information_complement`,
		`o`.`date_modification` AS `saisie_date_modification`,
		`o`.`date_creation` AS `saisie_date_creation` 
	from (`cel_obs` `o` 
		left join `cel_zones_geo` `zg` on((`o`.`ce_zone_geo` = `zg`.`id_zone_geo`))) 
	where `o`.`transmission` = 1 
		AND (`o`.`mots_cles_texte` NOT LIKE '%sensible%' OR `o`.`mots_cles_texte` IS NULL);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
