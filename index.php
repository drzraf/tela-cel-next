<?php
// TODO: http://www.redbeanphp.com/welcome
// http://fr.slideshare.net/vvaswani/creating-rest-applications-with-the-slim-microframework

// http://limonade-php.github.io/
// http://slimframework.com/
// http://silex.sensiolabs.org/
// http://flightphp.com/learn

require 'Slim/Slim/Slim.php';
require 'jrest/lib/Cel.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(['debug' => true, 'log.enabled' => false]);
$app->setName('CEL');;

$conf = parse_ini_file('../cel/jrest/jrest.ini.php', TRUE);


/* $app->get('/archive/:year', function ($year) {
		echo "You are viewing archives from $year";
		})->conditions(array('year' => '(19|20)\d\d')); */
// $app->get('/', function() use ($app) { die('c'); });
$app->get('/index.html', function () use($app) {
		$app->redirect('war/cel2.html');
		// if ($id_utilisateur != null) $requete_selection_observations .= 'WHERE ce_utilisateur = '.Cel::db()->proteger($id_utilisateur);
	});




$app->get('/InventoryObservationCount(/:user(/))', function ($user = '') use($app) {
		return $user ? $app->redirect("/obscount/$user") : $app->redirect("/obscount");
	});
// /jrest/InventoryObservationCount
$app->get('/obscount(/:user)', function ($user = NULL) {
		initCel();
		valid($user);
		print Cel::db()->query(sprintf('SELECT COUNT(1) as nb_obs FROM cel_obs %s',
									   $user ? ' WHERE ce_utilisateur = ' . Cel::db()->quote($user) : '' ))->fetchColumn();
		exit;
	});

// /jrest/InventoryObservationList/XXX
$app->get('/obslist/:user', function ($user) use($conf) {
		initCel();
		valid($user);
		require 'jrest/lib/RechercheObservation.php';
		require 'jrest/lib/GestionChampsEtendus.php';
		$searcher = new RechercheObservation($conf);
		$data = $searcher->formaterPourEnvoiCel($searcher->rechercherObservations($user, [] /*$_GET*/)->get());
		header("Content-Type: application/json; charset=utf-8");
		print json_encode($data);
		exit;
	});

// /jrest/NameMap/(bdtfx|isfan|bdtxa)/\d+
$app->get('/map/:ref/:num', function ($ref, $num) use($conf) {
		require 'jrest/services/NameMap.php';
		require 'jrest/lib/RechercheInfosTaxonBeta.php';
		$x = new NameMap($conf);
		print $x->obtenirCarteChorologie($ref, $num);
		exit;
	})->conditions([ 'ref' => '(bdtfx|isfan|bdtxa)', 'num' => '\d+'] );


$app->post('/logout', function () {
		session_destroy();
		setcookie("cel_id", "", 1, '/');
		exit; // $_COOKIE[$name] = ""; pas nécessaire si on die() now

	});
$app->post('/login', function () {
		initCel();
		valid($user);
		/* session_cache_limiter(false);
		   session_start(); */
		exit;
	});


// compat
$app->get('/User/', function () {
		initCel();
		$cred = Cel::db()->query(sprintf('SELECT * FROM cel_utilisateurs cu WHERE courriel = %s'.
										 ' AND mot_de_passe = %s',
										 Cel::db()->quote(@$_COOKIE['cel_name']),
										 Cel::db()->quote(@$_COOKIE['cel_password'])))->fetch();
		if(!$cred) {
			$cred = [ 'connecte' => false, 'id_utilisateur' => session_id(), 'courriel' => '',
					  'mot_de_passe' => '', 'nom' => '', 'prenom' => '', 'licence_acceptee' => false,
					  'preferences_utilisateur' => '', 'admin' => false ];
		}

		print json_encode($cred);
		exit;
	});




// auth
function isAdmin($id) {
	return in_array($id, explode(',',$GLOBALS['config']['jrest_admin']['admin']));
}

function valid($id) {
	if (isset($_SESSION['user']) && isset($_SESSION['user']['name']) && $_SESSION['user']['name'] == '') {
			//cas de la session temporaire, on ne fait rien de particulier
	}
	elseif (isset($_SESSION['user']) &&
			isset($_SESSION['user']['name']) &&
			!isAdmin($_SESSION['user']['name']) &&
			$_SESSION['user']['name'] != $id) {
		// cas d'usurpation d'identité
		exit( 'Accès interdit' );
	}
}

function initCel() {
	Cel::$bdd = new Bdd2(parse_ini_file('../cel/jrest/jrest.ini.php', TRUE),
						  'database_cel');
	session_start();
}

$app->run();
